/**
 * FileName: FileIO.java
 * Author: 5153
 * Created: December 4, 2017
 * Last Modified: December 6, 2017
 */

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

class FileStats{
  private Scanner input = null;
  private ArrayList <String> wordList=new ArrayList<String>();
  private HashSet <String> wordSet=new HashSet<String>();
  private ArrayList <Entry<String>> entryList=new ArrayList<Entry<String>>();

  private class Entry <T> implements Comparable<Entry<T>>{
    public T word;
    public int frequency;
    public Entry(T word, int f){
      this.word=word;
      frequency=f;
    }
    public int compareTo(Entry<T> e){
      // if this is larger returns > 0
      // if equal returns 0
      // if this is less than e returns < 0
      return e.frequency - this.frequency;
    }
  }

  public FileStats(String path) {

    // open the file to read from or print an error message and exit the program
    try {
      input = new Scanner(new File(path));
    } catch(FileNotFoundException e) {
      System.out.println("Error opening file..");
      System.exit(1);
    }

    // loop as long as there is another line to be read from the file
    while(input.hasNextLine()){

      // Separate each line into words and converts them to lowercase.
      // Filters out extra characters.
      wordList.addAll(Arrays.stream(input.nextLine()
          .split("\\W+"))
          .filter(t->!t.equals(""))
          .map(String::toLowerCase)
          .collect(Collectors.toList())
      );
    }
    wordSet.addAll(wordList);  // stores each unique word
    count();  // populates entryList and prints the top 4 words
  }

  /*
   * This method is supposed to
   * 1. find the frequency of each word in the file.
   * 2. display the four most frequent words and their frequencies.
   */
  private void count() {

    // create an Entry for each unique word and add it to entryList
    wordSet.forEach(word->entryList
        .add(new Entry<>(word, Collections.frequency(wordList,word))));
    Collections.sort(entryList);  // sort words by most frequent

    // print top 4 words
    for (int i = 0; i < 4; i++){
      System.out.println(entryList.get(i).word + " appears "
              + entryList.get(i).frequency + " time(s).");
    }
  }
}

public class FileIO {
  public static void main(String args[]) throws IOException {
    FileStats fs = new FileStats("./resources/basketball.txt");
  }
}